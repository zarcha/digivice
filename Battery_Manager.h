#ifndef _BATTERY_MANAGER_H_
#define _BATTERY_MANAGER_H_
 
#include <LiFuelGauge.h>

LiFuelGauge gauge(MAX17043);

class Battery_Manager {
    public:
        Battery_Manager(){
            gauge.reset();
            delay(200);
        }

        int getBatteryPercentage(){
            return round(((gauge.getVoltage() - batteryMin) / batteryMax ) * 100);
        }

        float getVoltage(){
            return gauge.getVoltage();
        }

    private:
        double batteryMin = 3.2;
        double batteryMax = 4.2 - batteryMin;
};

#endif //_BATTERY_MANAGER_H_
