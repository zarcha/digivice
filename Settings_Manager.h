#ifndef _SETTINGS_MANAGER_H_
#define _SETTINGS_MANAGER_H_

class Settings_Manager {

    public:
        int GetSpeedMultiplier(){
            return speedMultiplier;
        }

        int CanSleep(){
            return canSleep;
        }

        int GetFPS(){
            return fps;
        }

    private:
        int speedMultiplier = 1;
        int fps = 1000;
        bool canSleep = true;
    
};

#endif //_SETTINGS_MANAGER_H_