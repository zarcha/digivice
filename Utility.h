#pragma once

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

void printText(double textSize, int x, int y, String text){
  display.setTextSize(textSize);
  display.setTextColor(WHITE);
  display.setCursor(x, y);
  display.println(text);
}

enum BUTTON{
  TOP = 0,
  MIDDLE = 1,
  BOTTOM = 2
};

enum EVOLUTION{
  EGG = 0,
  BABY1 = 1,
  BABY2 = 2,
  CHILD = 3,
  ADULT = 4,
  PERFECT = 5
};

enum STATE {
  IDLE,
  PRESSED,
  RELEASED,
  HOLDING
};
