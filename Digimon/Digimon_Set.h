#ifndef _DIGIMON_SET_H_
#define _DIGIMON_SET_H_

#include "IDigimon.h"

#include "Egg.h"
#include "Botamon.h"
#include "Koromon.h"

//EGG
Egg *egg = new Egg();

//Baby 1
Botamon *botamon = new Botamon();

//Baby 2
Koromon *koromon = new Koromon();

//Child

//Adult

//Perfect

const int maxEvolutions = 3, maxDigimon = 1;

IDigimon *digimonSet[maxEvolutions][maxDigimon] {
  {egg},
  {botamon},
  {koromon}
};

#endif //_DIGIMON_SET_H_
