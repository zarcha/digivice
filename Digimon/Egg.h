#ifndef _EGG_H_
#define _EGG_H_

#include "IDigimon.h"

#include "../Sprites/Egg_Sprites.h"

class Egg : public IDigimon {
public:
  int GetMaxHunger() {
    return 0;
  }
  int GetMaxCareMistakes() {return 0;}
  int GetMinCareMistakes() {return 0;}
  int GetMaxTraining() {return 0;}
  int GetMinTraining() {return 0;}
  int GetMinWeight() {return 0;}
  int GetSleepTime() {return 0;}
  int GetMaxDP() {return 0;}
  long GetMinLife() {return 60000;}

  String GetName() {
    return "Botamon Egg";
  }

  int GetEvolution(){
    return EVOLUTION::EGG;
  }

  unsigned char *GetIdleLeft(){
    return botamon_egg;
  }

  unsigned char *GetIdleRight(){
    return botamon_egg;
  }

  unsigned char *GetMoveLeft(){
    return botamon_egg_small;
  }

  unsigned char *GetMoveRight(){
    return botamon_egg_small;
  }

private:

};

#endif //_EGG_H_
