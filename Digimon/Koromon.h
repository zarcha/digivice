#ifndef _KOROMON_H_
#define _KOROMON_H_

#include "./IDigimon.h"

#include "../Sprites/Koromon_Sprites.h"

class Koromon : public IDigimon {
public:
int GetMaxHunger() {return 16;}
int GetMaxCareMistakes() {return 999;}
int GetMinCareMistakes() {return 0;}
int GetMaxTraining() {return 999;}
int GetMinTraining() {return 0;}
int GetMinWeight() {return 10;}
int GetSleepTime() {return 0;}
int GetMaxDP() {return 0;}
long GetMinLife() {return 144000000;}

String GetName(){
  return "Botamon";
}

int GetEvolution(){
    return EVOLUTION::CHILD;
}

unsigned char *GetIdleLeft(){
  return koromon_idle;
}

unsigned char *GetIdleRight(){
  return koromon_idle;
}

unsigned char *GetMoveLeft(){
  return koromon_move;
}

unsigned char *GetMoveRight(){
  return koromon_move;
}

private:
};

#endif //_KOROMON_H_
