#ifndef _IDIGIMON_H_
#define _IDIGIMON_H_

#include "../Utility.h"
class IDigimon {

public:
  virtual int GetMaxHunger() = 0;
  virtual int GetMaxCareMistakes() = 0;
  virtual int GetMinCareMistakes() = 0;
  virtual int GetMinTraining() = 0;
  virtual int GetMaxTraining() = 0;
  virtual int GetMinWeight() = 0;
  virtual int GetSleepTime() = 0;
  virtual int GetMaxDP() = 0;
  virtual long GetMinLife() = 0;
  virtual String GetName() = 0;
  virtual int GetEvolution() = 0;
  virtual unsigned char *GetIdleLeft() = 0;
  virtual unsigned char *GetIdleRight() = 0;
  virtual unsigned char *GetMoveLeft() = 0;
  virtual unsigned char *GetMoveRight() = 0;
};

#endif // _IDIGIMON_H_
