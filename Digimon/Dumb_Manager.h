#ifndef _DUMB_MANAGER_H_
#define _DUMB_MANAGER_H_

#include "Digimon_Set.h"

class Dumb_Manager {
public:

  static Dumb_Manager* instance;

  static Dumb_Manager* GetInstance(){

    if(!instance){
      instance = new Dumb_Manager();
    }

    return instance;
  }

  void DecreaseHunger(){
    if(hunger != 0){
      hunger -= 1;
    }
  }

  bool IncreaseHunger(){
    if(hunger < GetDigimon()->GetMaxHunger()){
      hunger += 1;
      return true;
    }

    return false;
  }

  int GetHunger(){
    return hunger;
  }

  void DecreaseStrength(){
    if(strength != 0){
      strength -= 1;
    }
  }

  void IncreaseStrength(){
    strength += 1;
  }

  int GetStrength(){
    return strength;
  }

  void AddCareMistake(){
    careMistakes += 1;
  }

  void AddTrainings(){
    trainings += 1;
  }

  void ResetCareMistakes(){
    careMistakes = 0;
  }

  int GetCareMistakes(){
    return careMistakes;
  }

  int GetWeight(){
    return weight > 99 ? 99 : weight;
  }

  void IncreaseWeight(int amount){
    weight += amount;
  }

  void DecreaseWeight(int amount){
    if((weight - amount) >= GetDigimon()->GetMinWeight()){
      weight -= amount;
    }else{
      weight = GetDigimon()->GetMinWeight();
    }
  }

  int GetAge(){
    return (int)(86400000 / (lifeStart - millis()));
  }

  String GetName(){
    return GetDigimon()->GetName();
  }

  int GetEvolution(){
    return GetDigimon()->GetEvolution();
  }

  int GetCurrectDigimon(){
    return currentDigimon;
  }

  EVOLUTION GetCurrentEvolution(){
    return currentEvolution;
  }

  IDigimon *GetDigimon() {
    IDigimon **evolutionSet = digimonSet[currentEvolution];
    return evolutionSet[currentDigimon];
  }

  void UpdateStats(long currentTime){

    if(lifeStart == 0){
      long temp = currentTime;
      lifeStart = temp;
      evolutionStart = temp;
      SetDigimonStats();
    }else if((long)(currentTime - evolutionStart) >= GetDigimon()->GetMinLife() && currentEvolution < maxEvolutions){
      EvolveDigimon(currentTime);
      LifeTasks();
    }else if(currentEvolution != EVOLUTION::EGG && (long)(currentTime - lastLifeTask) >= lifeTaskRate){
      LifeTasks();
      lastLifeTask = currentTime - lifeTaskRate;
    }
  }

  void CheckCareStatus(bool fromLifeTask){
    if(GetHunger() == 0 || GetStrength() == 0){
      if(call && fromLifeTask){
        careMistakes += 1;
      }else{
        call = true;
      }

      //digitalWrite(13, HIGH);
    }else{
      call = false;
     // digitalWrite(13, LOW);
    }
  }

  void AddPoop(){
    if(poops < 6){
      poops += 1;
    }
  }

  void ClearPoop(){
    poops = 0;
  }

  int GetPoops(){
    return poops;
  }

private:

  //Time
  long evolutionStart = 0;
  long lifeStart = 0;

  long lastLifeTask = 0;
  long lifeTaskRate = 300000;

  //Current Digimon
  EVOLUTION currentEvolution = EVOLUTION::EGG;
  int currentDigimon = 0;

  //Stats
  int careMistakes = 0;
  int trainings = 0;
  int strength = 0;
  int hunger = 0;
  int weight = 0;
  int age = 0;

  int poops = 4;

  bool call = false;

  Dumb_Manager(){

  }

  void EvolveDigimon(long currentTime){
    IDigimon **evolutionSet = digimonSet[currentEvolution + 1];
    for(int i = 0; i < maxEvolutions; i++){
      if(evolutionSet[i] != NULL){
        if(evolutionSet[i]->GetMinTraining() <= trainings
          && evolutionSet[i]->GetMaxTraining() >= trainings
          && evolutionSet[i]->GetMinCareMistakes() <= careMistakes
          && evolutionSet[i]->GetMaxCareMistakes() >= careMistakes){
            currentEvolution = EVOLUTION((int)currentEvolution + 1);
            currentDigimon = i;

            SetDigimonStats();

            evolutionStart = currentTime;

            return;
          }
      }
    }
  }

  void SetDigimonStats(){
    careMistakes = 0;
    trainings = 0;

    if(weight < GetDigimon()->GetMinWeight()){
      weight = GetDigimon()->GetMinWeight();
    }
  }

  void LifeTasks(){
    DecreaseHunger();
    DecreaseStrength();

    CheckCareStatus(true);
  }
};

#endif //_DUMB_MANAGER_H_
