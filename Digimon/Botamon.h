#ifndef _BOTAMON_H_
#define _BOTAMON_H_

#include "IDigimon.h"

#include "../Sprites/Botamon_Sprites.h"

class Botamon : public IDigimon {
public:
int GetMaxHunger() {return 4;}
int GetMaxCareMistakes() {return 999;}
int GetMinCareMistakes() {return 0;}
int GetMinTraining() {return 0;}
int GetMaxTraining() {return 999;}
int GetMinWeight() {return 5;}
int GetSleepTime() {return -1;}
int GetMaxDP() {return 0;}
long GetMinLife() {return 3600000;}

String GetName(){
  return "Botamon";
}

int GetEvolution(){
    return EVOLUTION::CHILD;
}

unsigned char *GetIdleLeft(){
  return botamon_idle;
}

unsigned char *GetIdleRight(){
  return botamon_idle;
}

unsigned char *GetMoveLeft(){
  return botamon_move_left;
}

unsigned char *GetMoveRight(){
  return botamon_move;
}

private:
};

#endif //_BOTAMON_H
