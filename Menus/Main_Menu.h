#ifndef _MAIN_MENU_H_
#define _MAIN_MENU_H_

#include <math.h>

#include "Menu_Item_Interface.h"
#include "Menu_Manager.h"
#include "../Digimon/Dumb_Manager.h"
#include "../Time_Manager.h"
#include "../Settings_Manager.h"
#include "../Sprites/General_Sprites.h"
// #include "C:/Users/zarch/Documents/projects/digivice/Battery_Manager.h"
#include "/Users/zarch/Documents/projects/digivice/Battery_Manager.h"


class Main_Menu : public Menu_Item_Interface {

  public:
    Main_Menu(Menu_Manager menu_manager, Battery_Manager battery_manager, Settings_Manager settings_manager, Time_Manager time_manager) {
      menuManager = menu_manager;
      digimonManager = Dumb_Manager::GetInstance();
      timeManager = time_manager;
      settingsManager = settings_manager;
      batteryManager = battery_manager;
    }

    void MenuOptionShow(){

    }

    void HandleButton(STATE *states) {
      if(states[BUTTON::TOP] == STATE::PRESSED){
        if(settingTime){
          ChangeTime(BUTTON::TOP);
        }else if (!showingTime){
          menuManager.UpdateMenuOption();
        }
      }else if(states[BUTTON::MIDDLE] == STATE::PRESSED){
        if(menuManager.IsInMenu()){
          menuManager.SetActiveMenu();
        }else if(!settingTime){
          showingTime = !showingTime;
        }else if(settingTime){
          ChangeTime(BUTTON::MIDDLE);
        }
      }else if(states[BUTTON::BOTTOM] == STATE::PRESSED){
        if(settingTime){
          ChangeTime(BUTTON::BOTTOM);
        }else if(!showingTime){
          menuManager.BackOutOfMenu();
        }
      }else if(states[BUTTON::TOP] == STATE::HOLDING && states[BUTTON::BOTTOM] == STATE::HOLDING && showingTime && !settingTime){
        settingTime = true;
        showingTime = false;
      }
    }

    void Reset(){
      showingTime = false;
    }

    void LoopCall(long currentTime){
      if(!showingTime && !settingTime){
        MoveAndDisplayDigimon(currentTime);
        //DisplayPoop();
      } else if(showingTime) {
        if(timeManager.isTimeSet()){
          timeManager.UpdateTime(currentTime);
        }
        DisplayTime();
      } else if(settingTime){
        DisplayTime();
      }

      display.display();
    }

    void Init(){
      menuManager.SetActiveMenu();
    }

  private:
    Menu_Manager menuManager;
    Dumb_Manager *digimonManager;
    Time_Manager timeManager;
    Settings_Manager settingsManager;
    Battery_Manager batteryManager;

    bool showingTime = false;
    bool settingTime = false;
    int someNumber = 0;

    long lastAnimation = millis();
    int direction = 0; //0 = right, 1 = left
    int type = 0; //0 = idle, 1 = move

    int position  = 0; //8 positions

    bool settingHours = true;

    int currentlySetting = 0; //0 = AM, 1 = Hours, 2 = Minutes

    void ChangeTime(BUTTON button){
      switch (button)
      {
        case BUTTON::TOP:
          ChangeSetting();
          break;
        case BUTTON::MIDDLE:
          currentlySetting += currentlySetting == 2 ? -2 : 1;
          break;
        case BUTTON::BOTTOM:
          timeManager.isTimeSet(true);
          settingTime = false;
          showingTime = true;
          break;
        default:
          break;
      }
    }

    void ChangeSetting(){
      int tempAM = timeManager.isAM();
      int tempHours = timeManager.GetHours();
      int tempMinutes = timeManager.GetMinutes();

      switch (currentlySetting)
      {
        case 0:
          tempAM = !tempAM;
          break;
        case 1:
          tempHours += 1;
          break;
        case 2:
          tempMinutes += 1;
          break;
        default:
          currentlySetting = 0;
          ChangeSetting();
          break;
      }

      timeManager.SetTime(tempHours, tempMinutes, tempAM);
    }

    void DisplayTime(){
      String tempTime = (String)timeManager.Format(timeManager.GetHours()) + ":" + (String)timeManager.Format(timeManager.GetMinutes());
      String tempAM = timeManager.GetFormatedAM();
      int batteryCharge = batteryManager.getBatteryPercentage();

      display.clearDisplay();
      printText(3, 0, 40, tempAM);
      printText(3, 17, 0, tempTime);

      if(settingTime){
        printText(3, 70, 40, "SET");
      }else{
        printText(1, 110, 55, (String)batteryCharge + "%");
      }

      display.display();
    }
    
    void MoveAndDisplayDigimon(long currentTime){
      if((int)(currentTime - lastAnimation) >= settingsManager.GetFPS()){
        display.clearDisplay();

        int pixelPosition = position * 10;

        if(digimonManager->GetEvolution() == EVOLUTION::EGG){
          if(type == 0){
            display.drawBitmap(32, 0, digimonManager->GetDigimon()->GetIdleLeft(), 64, 64, 1);
          }else{
            display.drawBitmap(32, 0, digimonManager->GetDigimon()->GetMoveLeft(), 64, 64, 1);
          }
        }else if(direction == 0){
          if(type == 0){
            display.drawBitmap(pixelPosition, 0, digimonManager->GetDigimon()->GetIdleRight(), 64, 64, 1);
          }else{
            display.drawBitmap(pixelPosition, 0, digimonManager->GetDigimon()->GetMoveRight(), 64, 64, 1);
          }

          position++;
          if(position > 7){
            direction = 1;
            position = 7;
          }
        }else if(direction == 1){
          if(type == 0){
            display.drawBitmap(pixelPosition, 0, digimonManager->GetDigimon()->GetIdleLeft(), 64, 64, 1);
          }else{
            display.drawBitmap(pixelPosition, 0, digimonManager->GetDigimon()->GetMoveLeft(), 64, 64, 1);
          }

          position--;
          if(position < 0){
            direction = 0;
            position = 0;
          }
        }

        lastAnimation = currentTime;
        type = (type == 0) ? 1 : 0;

        //display.display();
      }
    }

    void DisplayPoop(){
      int row = 0;
      
      for(int i = 0; i < digimonManager->GetPoops() + 1;){
        if(row == 2) { row = 0; i++; }

        display.drawBitmap(128 - (i * 32), row * 32, poop, 32, 32, 1);

        row++;
      }
    }

};

#endif
