#ifndef _MENU_MANAGER_H_
#define _MENU_MANAGER_H_

#include "../Utility.h"
#include "Menu_Item_Interface.h"
#include "../Sprites/General_Sprites.h"
#include "../Digimon/Dumb_Manager.h"

class Menu_Manager {
  public:

    Menu_Manager(){
      digimonManager = Dumb_Manager::GetInstance();
    }

    void AddMenu(Menu_Item_Interface* menu, int position){
      menuOptions[position] = menu;
    }

    bool IsInMenu(){
      return currentMenu != currentOption;
    }

    void SetActiveMenu(){
      currentMenu = currentOption;
      menuOptions[currentMenu] -> Init();
    }

    void UpdateMenuOption(){
      if(digimonManager->GetEvolution() != EVOLUTION::EGG){
        currentOption += 1;
        if(currentOption >= maxMenus){
          currentOption = 1;
        }

        display.clearDisplay();

        menuOptions[currentOption] -> MenuOptionShow();

        display.drawBitmap(0, 20,  arrow_left, 24, 24, 1);
        display.drawBitmap(104, 20,  arrow_right, 24, 24, 1);

        display.display();
      }
    }

    int GetCurrentMenuIndex(){
      return currentMenu;
    }

    void HandleButton(STATE *states){
        menuOptions[currentMenu] -> HandleButton(states);
    }

    void BackOutOfMenu(){
      menuOptions[currentMenu] -> Reset();

      currentMenu = 0;
      currentOption = 0;
      display.clearDisplay();
    }

    void CallMenuLoop(long currentTime){
      if(currentMenu == currentOption) {
          menuOptions[currentMenu]->LoopCall(currentTime);
      }
    }

  private:

    Dumb_Manager *digimonManager;

    const static int maxMenus = 4;

    int currentMenu = 0;
    int currentOption = 0;

    Menu_Item_Interface *menuOptions[maxMenus];
};

#endif //_MENU_MANAGER_H_
