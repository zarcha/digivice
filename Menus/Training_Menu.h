#ifndef _TRAINING_MENU_H_
#define _TRAINING_MENU_H_

#include "Menu_Item_Interface.h"

#include "../Sprites/Training_Menu_Sprites.h"
#include "../Digimon/Dumb_Manager.h"
#include "../Settings_Manager.h"

class Training_Menu : public Menu_Item_Interface {

  public:
    Training_Menu(Menu_Manager menu_manager, Settings_Manager settings_manager)
    {
      digimonManager = Dumb_Manager::GetInstance();
      menuManager = menu_manager;
      settingsManager = settings_manager;
    }

    void MenuOptionShow(){
      display.drawBitmap(32, 0, training, 64, 64, 1);
    }

    void HandleButton(STATE *states) {
      if(states[BUTTON::TOP] == STATE::PRESSED){
        if(powerUp < maxPowerUp){
          powerUp += 1;
          UpdateProgress(powerUp);
          digimonManager->CheckCareStatus(false);
        }

        if(!trainingStarted){
          digimonManager->DecreaseWeight(2);
          digimonManager->IncreaseStrength();
          digimonManager->AddTrainings();
          trainingStarted = true;
        }
      }else if(states[BUTTON::MIDDLE] == STATE::PRESSED){
        Reset();
        Init();
      }else if(states[BUTTON::BOTTOM] == STATE::PRESSED){
        menuManager.BackOutOfMenu();
      }
    }

    void Reset(){
      timer = 5;
      powerUp = 0;
      timesPlayed = 0;
      digiAnimationState = 0;
      trainingStarted = false;
    }

    void LoopCall(long currentTime){
      if(trainingStarted){
        ShowPowerUpScreen(currentTime);
      }
    }

    void Init(){
      display.clearDisplay();

      printText(3, 0, 0, "Pow");
      printText(3, 96, 0, (String)timer);
      display.drawBitmap(0, 32, training_bar_open, 128, 32, 1);
      currentDigimon = digimonManager->GetCurrectDigimon();
      currentEvolution = digimonManager->GetCurrentEvolution();
      
      ShowPowerUpScreen(millis());
    }

  private:
    Dumb_Manager *digimonManager;
    Menu_Manager menuManager;
    Settings_Manager settingsManager;

    int currentDigimon;
    int currentEvolution;

    //Training
    bool trainingStarted = false;
    int timer = 5;
    int powerUp = 0; //max 14
    int lastPowerUp = 0;
    int maxPowerUp = 14;

    //Training Animations
    int digiAnimationState = 0;
    long lastAnimation = millis();
    int timesPlayed = 0;

    void ShowPowerUpScreen(long currentTime){
      if((int)(currentTime - lastAnimation) >= settingsManager.GetFPS()){
        if(timer <= 0){
          display.clearDisplay();
          if(timesPlayed == 0){
            display.drawBitmap(96, 0, digimonManager->GetDigimon()->GetMoveLeft(), 64, 64, 1);
            display.drawBitmap(0, 0, training_wall_standing, 32, 64, 1);
          }else if(timesPlayed < 5){
            display.drawBitmap(96, 0, digimonManager->GetDigimon()->GetIdleLeft(), 64, 64, 1);
            display.drawBitmap(88 - (timesPlayed * 14), 0, training_attack, 32, 32, 1);
            display.drawBitmap(0, 0, training_wall_standing, 32, 64, 1);
          }else if(timesPlayed == 5){
            display.drawBitmap(96, 0, digimonManager->GetDigimon()->GetMoveLeft(), 64, 64, 1);

            if(powerUp == maxPowerUp){
              display.drawBitmap(0, 40, training_wall_down, 32, 24, 1);
            }else{
              display.drawBitmap(0, 0, training_wall_standing, 32, 64, 1);
            }
          }else{
            Reset();
            Init();
          }

          timesPlayed += 1;
        } else if(trainingStarted) {
          timer -= 1;
          display.fillRoundRect(96, 0, 32, 32, 0, BLACK);
          printText(3, 96, 0, (String)timer);
        }

        display.display();
        lastAnimation = currentTime;
      }
    }

    void UpdateProgress(int num){
        display.fillRoundRect(((num - 1) * 8) + 10, 40, 4, 16, 0, WHITE);
        display.display();
    }

    void ShowAnimationScreen(){

    }

};

#endif //_TRAINING_MENU_H_
