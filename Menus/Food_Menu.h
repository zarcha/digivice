#ifndef _FOOD_MENU_H_
#define _FOOD_MENU_H_

#include "Menu_Item_Interface.h"

#include "../Sprites/Food_Menu_Sprites.h"
#include "../Digimon/Dumb_Manager.h"
#include "../Sprites/General_Sprites.h"
#include "../Settings_Manager.h"

class Food_Menu : public Menu_Item_Interface {

  public:
    Food_Menu(Menu_Manager menu_manager, Settings_Manager settings_manager)
    {
      digimonManager = Dumb_Manager::GetInstance();
      menuManager = menu_manager;
      settingsManager = settings_manager;
    }

    void MenuOptionShow(){
      display.drawBitmap(32, 0, food, 64, 64, 1);
    }

    void HandleButton(STATE *states) {
      if(states[BUTTON::TOP] == STATE::PRESSED && !feeding){
        ChangeSelection();
      }else if(states[BUTTON::MIDDLE] == STATE::PRESSED){
        if(feeding){
          int temp = option;
          Reset();
          option = (temp == 0) ? 1 : 0;;
          ChangeSelection();
        }else{
          if(option == 0){
            if(digimonManager->IncreaseHunger()){
              digimonManager->IncreaseWeight(1);
            }
          }else{
            digimonManager->IncreaseStrength();
            digimonManager->IncreaseWeight(2);
          }

          digimonManager->CheckCareStatus(false);
          feeding = true;
        }
      }else if(states[BUTTON::BOTTOM] == STATE::PRESSED){
        menuManager.BackOutOfMenu();
      }
    }

    void Reset(){
      option = 1;
      timesPlayed = 0;
      feeding = false;
    }

    void LoopCall(long currentTime){
      if(feeding){
        Feed(currentTime);
      }
    }

    void Init(){
      ChangeSelection();
    }

    private:
      Dumb_Manager *digimonManager;
      Menu_Manager menuManager;
      Settings_Manager settingsManager;

      int option = 1;

      //Feeding
      bool feeding = false;
      int digiAnimationState = 0;
      long lastAnimation = millis();
      int timesPlayed = 0;

      void ShowOptions(){
          display.drawBitmap(80, 0, bread_full, 32, 32, 1);
          display.drawBitmap(80, 32, meat_full, 32, 32, 1);
      }

      void ChangeSelection(){
        option = (option == 0) ? 1 : 0;

        display.clearDisplay();
        ShowOptions();
        display.drawBitmap(32, (option * 32) + 8,  arrow_right, 24, 24, 1);
        display.display();
      }

      void Feed(long currentTime){
        if(timesPlayed < 4 && (int)(currentTime - lastAnimation) >= settingsManager.GetFPS()){
          display.clearDisplay();

          if(option == 0){
            display.drawBitmap(64, 0, bread_full, 32, 32, 1);
          }else if (option == 1){
            display.drawBitmap(64, 0, meat_full, 32, 32, 1);
          }

          if(digiAnimationState == 0){
            display.drawBitmap(0, 0, digimonManager->GetDigimon()->GetIdleRight(), 64, 64, 1);
          }else if(digiAnimationState == 1){
            display.drawBitmap(0, 0, digimonManager->GetDigimon()->GetMoveRight(), 64, 64, 1);
          }

          display.display();
          lastAnimation = currentTime;
          digiAnimationState = (digiAnimationState == 0) ? 1 : 0;
          timesPlayed += 1;
        }else if(timesPlayed >= 4){
          int temp = option;
          Reset();
          option = (temp == 0) ? 1 : 0;;
          ChangeSelection();
        }
      }

};

#endif //_FOOD_MENU_H_