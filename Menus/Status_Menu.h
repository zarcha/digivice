#ifndef _STATUS_MENU_H_
#define _STATUS_MENU_H_

#include "Menu_Item_Interface.h"
#include "../Digimon/Dumb_Manager.h"
#include "../Sprites/Status_Menu_Sprites.h"
#include "Menu_Manager.h"

class Status_Menu : public Menu_Item_Interface {

  public:
    Status_Menu(Menu_Manager menu_manager)
    {
      menuManager = menu_manager;
    }

    void MenuOptionShow(){
      display.drawBitmap(32, 0, status, 64, 64, 1);
    }

    void HandleButton(STATE *states) {
      if(states[BUTTON::TOP] == STATE::PRESSED){
        currentPage += 1;
        DisplayPage();
      }else if(states[BUTTON::MIDDLE] == STATE::PRESSED){
        currentPage = 0;
        DisplayPage();
      }else if(states[BUTTON::BOTTOM] == STATE::PRESSED){
        menuManager.BackOutOfMenu();
      }
    }

    void Reset(){
      currentPage = 0;
    }

    void LoopCall(long currentTime){

    }

    void Init(){
      DisplayPage();
    }

    private:
      Menu_Manager menuManager;
      Dumb_Manager *dumbManager = Dumb_Manager::GetInstance();

      int currentPage = 0;
      int maxPages = 3; //this being one larger than the highest amount

      void DisplayPage(){
        display.clearDisplay();
        switch (currentPage) {
          case 0:
            ShowAgeWeight();
            break;
          case 1:
            ShowHunger();
            break;
          case 2:
            ShowStrength();
            break;
          default:
            currentPage = 0;
            DisplayPage();
            break;
        }
        
        display.display();
      }

      void ShowAgeWeight(){
        display.drawBitmap(0, -8, age, 32, 32, 1);
        display.drawBitmap(0, 32, weight, 32, 32, 1);
        printText(2.5, 40, 5, (String)(dumbManager->GetAge()) + "yr.");
        printText(2.5, 40, 40, (String)(dumbManager->GetWeight()) + "lb.");
      }

      void ShowHunger(){
        printText(2.5, 0, 0, "Hungry");
        int hunger = dumbManager->GetHunger();
        for(int i = 0; i < 4; i++){
            if(hunger < (i + 1)){
              display.drawBitmap(i * 32, 26, emptyHeart, 32, 32, 1);
            }else{
              display.drawBitmap(i * 32, 26, fullHeart, 32, 32, 1);
            }

        }
      }

      void ShowStrength(){
        printText(2.5, 0, 0, "Strength");
        int strength = dumbManager->GetStrength();
        for(int i = 0; i < 4; i++){
            if(strength < (i + 1)){
              display.drawBitmap(i * 32, 26, emptyHeart, 32, 32, 1);
            }else{
              display.drawBitmap(i * 32, 26, fullHeart, 32, 32, 1);
            }

        }
      }
};

#endif //_STATUS_MENU_H_