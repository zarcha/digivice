#ifndef _MENU_ITEM_INTERFACE_
#define _MENU_ITEM_INTERFACE_

#include "../Utility.h"

class Menu_Item_Interface
{
  public:
    virtual void MenuOptionShow() = 0;
    virtual void HandleButton(STATE *states) = 0;
    virtual void Reset() = 0;
    virtual void LoopCall(long currentTime) = 0;
    virtual void Init() = 0;

  private:
};

#endif //_MENU_ITEM_INTERFACE_