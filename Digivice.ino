#include <avr/sleep.h>

#include "./Utility.h"
#include "./Input_Manager.h"
#include "./Menus/Menu_Manager.h"
#include "./Menus/Food_Menu.h"
#include "./Menus/Status_Menu.h"
#include "./Menus/Main_Menu.h"
#include "./Menus/Training_Menu.h"
#include "./Digimon/Dumb_Manager.h"
#include "./Time_Manager.h"
#include "./Settings_Manager.h"
// #include "C:/Users/zarch/Documents/projects/digivice/Battery_Manager.h"
#include "/Users/zarch/Documents/projects/digivice/Battery_Manager.h"

Dumb_Manager *Dumb_Manager::instance = NULL;

Menu_Manager menuManager;
Dumb_Manager *digimonManager;
Time_Manager timeManager;
Input_Manager inputManager;
Settings_Manager settingsManager;
Battery_Manager batteryManager;

Main_Menu *mainMenu;
Status_Menu *statusMenu;
Food_Menu *foodMenu;
Training_Menu *trainingMenu;

long updateInterval = 60000; //5mins
long lastUpdate = millis();

bool interaction = false;
long systemIdle = millis();
long systemIdleThreshold = 60000;

bool sleeping = false;

void setup() {
  Wire.begin();
  Serial.begin(9600);

  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);
  display.clearDisplay();
  display.display();

  digimonManager = Dumb_Manager::GetInstance();

  mainMenu = new Main_Menu(menuManager, batteryManager, settingsManager, timeManager);
  statusMenu = new Status_Menu(menuManager);
  foodMenu = new Food_Menu(menuManager, settingsManager);
  trainingMenu = new Training_Menu(menuManager, settingsManager);

  menuManager.AddMenu(mainMenu, 0);
  menuManager.AddMenu(statusMenu, 1);
  menuManager.AddMenu(foodMenu, 2);
  menuManager.AddMenu(trainingMenu, 3);

  display.dim(true);
}

void loop() {
  STATE *states = inputManager.Check();

  if(states[BUTTON::TOP] != STATE::IDLE || states[BUTTON::MIDDLE] != STATE::IDLE || states[BUTTON::BOTTOM] != STATE::IDLE){
    interaction = false;
  }else{
    interaction = true;
  }

  if(sleeping == true){
    
    Serial.println("Sleeping");

    set_sleep_mode(SLEEP_MODE_ADC);
    noInterrupts();
    sleep_enable();
    interrupts();
    sleep_cpu();
    sleep_disable();
    

    if(!interaction){
      display.ssd1306_command(SSD1306_DISPLAYON);
      sleeping = false;
      return;
    }

  }else if(sleeping == false){
    int currentTime = millis() * settingsManager.GetSpeedMultiplier();

    if(timeManager.isTimeSet() == true){
      digimonManager->UpdateStats(currentTime);
    }
    lastUpdate = currentTime;

    if(!interaction) { 
      menuManager.HandleButton(states);
      systemIdle = currentTime; 
    }

    if((long)(currentTime - systemIdle) >= systemIdleThreshold && settingsManager.CanSleep()){
      display.ssd1306_command(SSD1306_DISPLAYOFF);
      sleeping = true;
      return;
    }

    menuManager.CallMenuLoop(currentTime);
  }
}
