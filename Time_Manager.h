#ifndef _TIME_MANAGER_H_
#define _TIME_MANAGER_H_

class Time_Manager {
  public:
    void SetTime(int newHours, int newMinutes, bool amPM){
      hours = newHours;
      minutes = newMinutes;

      if(minutes > 59){
        minutes -= 59;
        hours += 1;
      }

      if(hours > 12){
        hours -= 12;
      }

      refMillis = millis();
      am = amPM;
    }

    int GetHours(){
      return hours;
    }

    int GetMinutes(){
      return minutes;
    }

    bool isAM(){
      return am;
    }

    String Format(int time){
      if(time < 10){
        return "0" + (String)time;
      }

      return (String)time;
    }

    String GetFormatedAM(){
      if(am){
        return "AM";
      }

      return "PM";
    }

    void isTimeSet(bool state){
      timeSet = state;
    }

    bool isTimeSet(){
      return timeSet;
    }

    void UpdateTime(long currentTime){
      long mils = currentTime - refMillis;
      int minutestoAdd = 0;
      int hoursToAdd = (int)(mils / refHours);
      mils = mils - (refHours * hoursToAdd);
      if(mils > refMinute){
        minutestoAdd = (int)((mils - (refHours * hoursToAdd)) / refMinute);
        mils = mils - (refMinute * minutestoAdd);
      }

      hours += hoursToAdd;
      minutes += minutestoAdd;

      if(minutes > 59){
        minutes -= 59;
        hours += 1;
      }

      if(hours > 12){
        hours -= 12;
      }

      refMillis = currentTime - mils;
    }

  private:
    long refMillis = millis();
    bool timeSet = false;
    bool am = true;
    int hours = 12;
    int minutes = 0;

    long refHours = 3600000;
    long refMinute = 60000;

};

#endif //_TIME_MANAGER_H_
