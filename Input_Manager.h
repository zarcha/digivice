#ifndef _INPUT_MANAGER_H_
#define _INPUT_MANAGER_H_

#include "Utility.h"

class Input_Manager {

  public:
    Input_Manager(){
      for(int i = 0; i < buttonOutArraySize; i++){
        pinMode(buttonOutArray[i], OUTPUT);
      }

      for(int i = 0; i < buttonInArraySize; i++){
        pinMode(buttonInArray[i], INPUT_PULLUP);
      }
    }

    STATE *Check(){
      for(int i = 0; i < buttonOutArraySize; i++){
        digitalWrite(buttonOutArray[i], LOW);

        for(int j = 0; j < buttonInArraySize; j++){

          //This will probably only work on single columns
          states[j] = (STATE)getKeyState(j);
        }

        digitalWrite(buttonOutArray[i], HIGH);
      }

      return states;
    }

  private:
    const static int buttonInArraySize = 3; //Num of columns
    int buttonInArray[buttonInArraySize] = { 12, 10, 11 }; //Pins used to read from on baord
    const static int buttonOutArraySize = 1; //Num of rows
    int buttonOutArray[buttonOutArraySize] = { 9 }; //Pins used to write to on board

    const int buttonBounce = 75; //Millis used for time it would take to press and release on a normal button press 
    bool currentButtonState[buttonInArraySize] = { false, false, false }; //True/False state of the buttons currently
    long currentButtonDownTime[buttonInArraySize] = { 0, 0, 0 }; //How long each button has been down

    //This is being used to send back when states are checked
    STATE states[buttonInArraySize] = { STATE::IDLE, STATE::IDLE, STATE::IDLE};

    int getKeyState(int button){
      long elapsedTime = 0;
      int returnState = 0;

      if(currentButtonDownTime[button] != 0){
        elapsedTime = millis() - currentButtonDownTime[button];
      }

      int currentState = digitalRead(buttonInArray[button]);
      long currentButtonStateValue = false;

      if(currentState == LOW ){
        if(currentButtonState[button] == false && currentButtonDownTime[button] == 0){
          currentButtonDownTime[button] = millis();
          returnState = STATE::PRESSED;
          currentButtonState[button] = true;
        }else if(currentButtonState[button] == true){
          returnState = STATE::HOLDING;
        }
      }else if(currentState == HIGH){
        currentButtonStateValue = currentButtonState[button];

        if(currentButtonStateValue == true && elapsedTime > buttonBounce){
          currentButtonDownTime[button] = 0;
          currentButtonState[button] = 0;
          currentButtonState[button] = false;
          returnState = STATE::RELEASED;
        }else if(currentButtonStateValue == false) {
          returnState = STATE::IDLE;
        }
      }

      return returnState;
    }

};

#endif //_INPUT_MANAGER_H_
